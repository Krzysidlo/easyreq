# EasyReq

### Python package for easy `requirements.txt` file generating or altering

    pip install easyreq
    
    easyreq --help


## Project searching

It will find all packages imports used in files in project, check installed version of the package using `pip freeze` and generate `requirements.txt` file based on that 

**If you want to search current directory you are in**

    :/home/user/projectName$ easyreq
    
**Or you can specify a path to a project you want to use it for**

    :/home/user/projectName$ easyreq /home/user/someOtherProjectName/

## Altering already existing requirements.txt

If a package in rquirements has specified version (eg. `urllib3==1.25.7`) it will be changed as a minimum (eg. `>=1.25.7`) and added a maximum one version higher (eg. `<2`)  
If a package has no version specified it will try to find installed one using `pip freeze` and add it as a minimum and add a maximum one version higher

**In order to alter `requirements.txt` file**

    :/home/user/projectName$ easyreq /home/user/projectName/requirements.txt

Or

    :/home/user/projectName$ easyreq requirements.txt
